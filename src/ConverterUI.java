import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * ConverterUI will show information from convert.
 * @author Jidapar Jettananurak
 *
 */
public class ConverterUI extends JFrame
{

	/**
	 * this is attribute
	 */
	private JButton convertButton;
	private JButton clearButton;
	private JLabel label;
	public JTextField inputField1;
	public JTextField inputField2;
	public JComboBox unitComboBox1;
	public JComboBox unitComboBox2;
	public UnitConverter unitconverter;
	
	/**
	 * Main method is run the UI.
	 * @param args
	 */
	public static void main(String[]args){
		UnitConverter uc = new UnitConverter();
		ConverterUI cu = new ConverterUI(uc);
	}
	
	/**
	 * Constructer create UI.
	 * @param uc unitconverter.
	 */
	public ConverterUI( UnitConverter uc ) {
		this.unitconverter = uc;
		this.setTitle("Length Converter");
		this.setVisible(true);
		this.setDefaultCloseOperation( EXIT_ON_CLOSE );
		initComponents( );
	}

	/**
	 * initialize components in the window
	 */
	private void initComponents() {

		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout( );
		contents.setLayout( layout );
		
		inputField1 = new JTextField(10);
		inputField2 = new JTextField(10);
		inputField2.setEditable(false);
		
		convertButton = new JButton("Convert");
		clearButton = new JButton("Clear");
		
		unitComboBox1 = new JComboBox<Unit>( unitconverter.getUnits() );
		unitComboBox2 = new JComboBox<Unit>( unitconverter.getUnits() );
		
		label = new JLabel("=");
		
		contents.add(inputField1);
		contents.add(unitComboBox1);
		contents.add(label);
		contents.add(inputField2);
		contents.add(unitComboBox2);
		contents.add(convertButton);
		contents.add(clearButton);
		
		ActionListener listener = new ConvertButtonListener( );
		convertButton.addActionListener( listener );
		inputField1.addActionListener(listener);
		
		ActionListener listener2 = new ClearButtonListener( );
		clearButton.addActionListener( listener2 );
		
		this.pack(); 
	}
	
	/**
	 * AcctionListenner for convert.
	 * @author Jidapar Jettananurak
	 *
	 */
	class ConvertButtonListener implements ActionListener {

		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			String s = inputField1.getText().trim();
			double result = 0;
			
			if(s.length() == 0){
				JOptionPane op = new JOptionPane();
				op.showMessageDialog(inputField1, "Don't have a number!");
				return;
			}
			else if ( s.length() > 0 ) {
				try{
					double value = Double.valueOf( s );
					Unit unit1 = (Unit) unitComboBox1.getSelectedItem( );
					Unit unit2 = (Unit) unitComboBox2.getSelectedItem( );
					result = unitconverter.convert( value, unit1 , unit2 );
				}catch( NumberFormatException nfe ) {
					JOptionPane op = new JOptionPane();
					op.showMessageDialog(inputField1, "Not a number!");
					return;
				}
				
				inputField2.setText( result + "" );
			}
		}
	}
	
	/**
	 * AcctionListenner for clear.
	 * @author Jidapar Jettananurak
	 *
	 */
	class ClearButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			inputField1.setText(null);
			inputField2.setText(null);
		}
	}
}