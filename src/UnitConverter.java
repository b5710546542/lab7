
public class UnitConverter {

	public double convert(double amount , Unit fromUnit , Unit toUnit){
		return fromUnit.convertTo(amount, toUnit);	
	}
	
	public Unit[] getUnits(){
		return Length.values();
	}
}
